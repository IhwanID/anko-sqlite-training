package id.ihwan.sqlitetraining

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_list_karyawan.*
import org.jetbrains.anko.db.classParser
import org.jetbrains.anko.db.select

class ListKaryawanActivity : AppCompatActivity() {

    private var karyawan = ArrayList<Karyawan>()
    private lateinit var adapter: KaryawanAdapter


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list_karyawan)


        adapter = KaryawanAdapter(this, karyawan)
        recyclerView.adapter =  adapter
        showFavorite()
        recyclerView.layoutManager = LinearLayoutManager(this)



//        database.use {
//            select("TABLE_KARYAWAN", "NAMA" ).exec {
//                parseList(StringParser).forEach {
//                    println(it)
//                    Toast.makeText(this@ListKaryawanActivity, "Nama "+ it, Toast.LENGTH_SHORT).show()
//                }
//            }
//        }


    }

    private fun showFavorite(){
        database.use {
            karyawan.clear()
            val result = select(Karyawan.TABLE_KARYAWAN)
            val karyawans = result.parseList(classParser<Karyawan>())
            Toast.makeText(this@ListKaryawanActivity, karyawans.toString(), Toast.LENGTH_SHORT).show()
            karyawan.addAll(karyawans)
            adapter.notifyDataSetChanged()
        }
    }
}
