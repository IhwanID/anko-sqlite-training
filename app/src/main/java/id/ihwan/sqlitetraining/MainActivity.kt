package id.ihwan.sqlitetraining

import android.database.sqlite.SQLiteConstraintException
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import org.jetbrains.anko.db.insert
import org.jetbrains.anko.db.update
import org.jetbrains.anko.startActivity

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val oldName = intent.getStringExtra("oldName")

        simpanData.setOnClickListener {
            addDataKaryawan()
        }

        lihatData.setOnClickListener {
            startActivity<ListKaryawanActivity>()
        }

        if (oldName.isNullOrBlank()){
            updateData.isEnabled = false
        }else{
            simpanData.isEnabled = false
        }


        updateData.setOnClickListener {
            database.use {
                update(Karyawan.TABLE_KARYAWAN,
                    Karyawan.NAMA to inputNama.text.toString(),
                    Karyawan.JABATAN to inputJabatan.text.toString(),
                    Karyawan.ASAL to inputAsal.text.toString())
                    .whereArgs("${Karyawan.NAMA} = {nama}",
                        "nama" to oldName)
                    .exec()
            }

            Toast.makeText(this, "Data Diupdate", Toast.LENGTH_SHORT).show()
        }

    }

    private fun addDataKaryawan(){
        try {
            database.use {
                insert(Karyawan.TABLE_KARYAWAN,
                    Karyawan.NAMA to inputNama.text.toString(),
                    Karyawan.JABATAN to inputJabatan.text.toString(),
                    Karyawan.ASAL to inputAsal.text.toString())
            }
            Toast.makeText(this, "Data Ditambahkan", Toast.LENGTH_SHORT).show()
        } catch (e: SQLiteConstraintException){
            Toast.makeText(this, "Gagal "+ e.localizedMessage, Toast.LENGTH_SHORT).show()
        }
    }




}
